"""Volume 3: Web Scraping.
Christopher Cook
MATH 406R
09/28/2108
"""

import requests
import os
import re
from bs4 import BeautifulSoup
from matplotlib import pyplot as plt

# Problem 1
def prob1():


    
    if os.path.isfile('example.html'):
        pass
    else:
        response = requests.get('http://www.example.com')
        with open('example.html', 'w') as file:
            file.write(response.text)
        print('requested!')
        

# Problem 2
def prob2():
    """Examine the source code of http://www.example.com. Determine the names
    of the tags in the code and the value of the 'type' attribute associated
    with the 'style' tag.

    Returns:
        (set): A set of strings, each of which is the name of a tag.
        (str): The value of the 'type' attribute in the 'style' tag.
    """
    #open file
    with open('example.html', 'r') as file:
        
        #get all the lovely text into a string
        text = file.read()
        
        #regular expression to match tag names
        re1 = re.compile(r'<([A-Za-z]+) ?.*?>')
        
        #regular expresion to match style type attribute
        re2 = re.compile(r'<style type="(.*?)">')
        
        #Grab the associated values, and unbox the styletype attribute
        tags = re1.findall(text)
        styletype = re2.findall(text)[0]
        
        #make tags a set
        stags = set(tags)
        
        return stags, styletype


# Problem 3
def prob3(code):
    """Return a list of the names of the tags in the given HTML code."""
    
    #make soup
    ugly_soup = BeautifulSoup(code,'html.parser')
    
    #get all tags
    tags = ugly_soup.find_all(True)
    
    #extract names
    
    #NOTE that the use of a list here is intentional since that's what the 
    #prompt asked for. Uniquness of tags wasn't, so I'm assuming you want 
    #duplicate tag names
    names = []
    for tag in tags:
        names.append(tag.name)

    return names



# Problem 4
def prob4(filename="example.html"):
    """Read the specified file and load it into BeautifulSoup. Find the only
    <a> tag with a hyperlink and return its text.
    """
    #open file and make soup
    with open(filename, 'r') as file:
        text = file.read()
        soup = BeautifulSoup(text,'html.parser')
        
        #find all a tags
        atags = soup.find_all(name='a')
        
        #get the href for the atag and assert that there is really only one
        found = False
        for tag in atags:
            if hasattr(tag, 'attrs') and 'href' in tag.attrs:
                if not found:
                    out_tag = tag
                else:
                    raise NotImplementedError("Your Test case doesn't comply with the specifications of this problem")
        
        return out_tag.string


# Problem 5
def prob5(filename="san_diego_weather.html"):
    """Read the specified file and load it into BeautifulSoup. Return a list
    of the following tags:

    1. The tag containing the date 'Thursday, January 1, 2015'.
    2. The tags which contain the links 'Previous Day' and 'Next Day'.
    3. The tag which contains the number associated with the Actual Max
        Temperature.

    Returns:
        (list) A list of bs4.element.Tag objects (NOT text).
    """
    
    #open file and make soup
    with open(filename, 'r') as file:
        text = file.read()
        
        soup = BeautifulSoup(text,'html.parser')
        
        #generate tag with date
        thurs = soup.find(string='Thursday, January 1, 2015').parent
        
        #generate tags for prev and next links
        prev_next = soup.find_all(class_='previous-link') + soup.find_all(class_='next-link')
        #generate tag of table
        table = soup.find_all(class_='responsive airport-history-summary-table')
        
        #assert that there is only one table that we found
        assert len(table) == 1
        table = table[0]
        
        #There's really not a good way to navigate the table except through this
        #navigate table to desired entry
        max_temp = table.contents[3].contents[5].contents[3].contents[1].contents[0]
    
    return [thurs, prev_next, max_temp]


# Problem 6
def prob6(filename="large_banks_index.html"):
    """Read the specified file and load it into BeautifulSoup. Return a list
    of the tags containing the links to bank data from September 30, 2003 to
    December 31, 2014, where the dates are in reverse chronological order.

    Returns:
        (list): A list of bs4.element.Tag objects (NOT text).
    """
    #open file and make soup
    with open(filename) as file:
        text = file.read()
        soup = BeautifulSoup(text,'html.parser')
        
        #create regular expression matching good dates
        re1 = re.compile(r'(January|February|March|April|May|June|July|August|September|October|November|December) \d\d, 20(03|04|05|06|07|08|09|10|11|12|13|14)')
        
        #get tags
        links = soup.find_all(href=True, string=re1)
        
        return links

#define sorting functions
def dom_sorter(line):
    return line['dom'], line['name']

def forg_sorter(line):
    return line['forg'], line['name']

# Problem 7
def prob7(filename="large_banks_data.html"):
    """Read the specified file and load it into BeautifulSoup. Create a single
    figure with two subplots:

    1. A sorted bar chart of the seven banks with the most domestic branches.
    2. A sorted bar chart of the seven banks with the most foreign branches.

    In the case of a tie, sort the banks alphabetically by name.
    """
    #open file and make soup
    with open(filename) as file:
        text = file.read()
        soup = BeautifulSoup(text,'html.parser')
        
        #find table
        table = soup.find(cellspacing='1', cellpadding='7', rules='GROUPS', frame='BOX', border = '1')
        
        #get to the correct part of the table
        rows = table.contents[3].contents
        
        #extract data from table that we need
        data = []
        for row in rows:
            if hasattr(row, 'contents') and len(row.contents) > 20:
                
                line = {}
                line['name'] = row.contents[1].string
                line['dom'] = row.contents[19].string
                line['forg'] = row.contents[21].string
                if line['forg'] != '.' and line['dom'] != '.':
                    line['forg'] = int(line['forg'].replace(',',''))
                    line['dom'] = int(line['dom'].replace(',',''))
                    data.append(dict(line))
        
        #Sort data by foreign branches and domestic branches, take top 7
        data.sort(reverse=True, key=dom_sorter)
                
        high_dom = data[:7]
        
        data.sort(reverse=True, key=forg_sorter)
          
        high_forg = data[:7]
        
        #resort so graphs will be in order
        high_dom.sort(key=dom_sorter)
        high_forg.sort(key=forg_sorter)
        
        #sepearte data into lists required for formatting
        domestic_amount = []
        for dom in high_dom:
            domestic_amount.append(dom['dom'])
        domestic_labels = []
        for dom in high_dom:
            domestic_labels.append(dom['name'])
        foreign_amount = []
        for forg in high_forg:
            foreign_amount.append(forg['forg'])
        foreign_labels = []
        for forg in high_forg:
            foreign_labels.append(forg['name'])
        
        # Plot the results.
        fig, [ax1, ax2] = plt.subplots(2,1, figsize=(12,6), sharex=True)
    
        ax1.barh(range(7), domestic_amount, tick_label=domestic_labels)
        ax1.set_title("Banks with the Most Domestic Branches")
        ax1.set_axisbelow(True)
        ax1.grid(axis='x')
    
        ax2.barh(range(7),  foreign_amount, tick_label=foreign_labels)
        ax2.set_title("Banks with the Most Foreign Branches")
        ax2.set_axisbelow(True)
        ax2.grid(axis='x')
    
        plt.tight_layout()
        plt.show()
            


