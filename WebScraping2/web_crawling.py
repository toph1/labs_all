"""Volume 3: Web Scraping 2.
<Name>
<Class>
<Date>
"""

import re
import time
import requests
from bs4 import BeautifulSoup
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.common.exceptions import NoSuchElementException

# libraries
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
 

# Problem 1
def scrape_books(start_page = "index.html"):
    """ Crawl through http://books.toscrape.com and extract fiction data"""

    # Initialize variables, including a regex for finding the 'next' link.
    base_url="http://books.toscrape.com/catalogue/category/books/fiction_10/"
    titles = []
    page = base_url + start_page                # Complete page URL.
    next_page_finder = re.compile(r"next")      # We need this button.

    current = None

    for _ in range(4):
        while current == None:  # Try downloading until it works.
            # Download the page source and PAUSE before continuing.
            page_source = requests.get(page).text
            time.sleep(1)       # PAUSE before continuing.
            soup = BeautifulSoup(page_source, "html.parser")
            current = soup.find_all(class_="product_pod")

        # Navigate to the correct tag and extract title
        for book in current:
            for child in book.children:
                if hasattr(child, 'class_') and hasattr(child, 'p') and hasattr(child.p, 'string'):
                    titles.append(child.p.string)

        # Find the URL for the page with the next data.
        if "page-4" not in page:
            new_page = soup.find(string=next_page_finder).parent["href"]
            page = base_url + new_page      # New complete page URL.
            current = None
    
    prices = []
    for price in titles:
        prices.append(float(price[2:]))
    return sum(prices)/len(prices)



# Problem 2
def bank_data():
    """Crawl through the Federal Reserve site and extract bank data."""
    # Compile regular expressions for finding certain tags.
    link_finder = re.compile(r"December 31, ((201[0-9])|(200[4-9]))$")
    chase_bank_finder = re.compile(r"^JPMORGAN CHASE BK")
    amer_bank_finder = re.compile(r"^BANK OF AMER NA/BANK OF AMER")
    wells_bank_finder = re.compile(r"^WELLS FARGO BK NA/WELLS FARGO")

    # Get the base page and find the URLs to all other relevant pages.
    base_url="https://www.federalreserve.gov/releases/lbr/"
    base_page_source = requests.get(base_url).text
    base_soup = BeautifulSoup(base_page_source, "html.parser")
    link_tags = base_soup.find_all(name='a', href=True, string=link_finder)
    pages = [base_url + tag.attrs["href"] for tag in link_tags]


    # Crawl through the individual pages and record the data.
    chase_assets = []
    amer_assets = []
    wells_assets= []
    for page in pages:
        time.sleep(1)               # PAUSE, then request the page.
        soup = BeautifulSoup(requests.get(page).text, "html.parser")

        # Find the tag corresponding to Chase Banks's consolidated assets.
        temp_tag = soup.find(name="td", string=chase_bank_finder)
        for _ in range(10):
            temp_tag = temp_tag.next_sibling
        # Extract the data, removing commas.
        chase_assets.append(int(temp_tag.string.replace(',', '')))
        
        # Find the tag corresponding to Chase Banks's consolidated assets.
        temp_tag = soup.find(name="td", string=amer_bank_finder)
        for _ in range(10):
            temp_tag = temp_tag.next_sibling
        # Extract the data, removing commas.
        amer_assets.append(int(temp_tag.string.replace(',', '')))

        # Find the tag corresponding to Chase Banks's consolidated assets.
        temp_tag = soup.find(name="td", string=wells_bank_finder)
        for _ in range(10):
            temp_tag = temp_tag.next_sibling
        # Extract the data, removing commas.
        wells_assets.append(int(temp_tag.string.replace(',', '')))

    years = ['2017','2016','2015','2014','2013','2012','2011','2010','2009','2008','2007','2006','2005','2004']

    # Data
    df=pd.DataFrame({'years':years, 'Chase':chase_assets, 'Wells_Fargo': wells_assets, 'Bank_of_America': amer_assets})
     
    # multiple line plot
    plt.plot( 'years', 'Chase', data=df, marker='o', markerfacecolor='blue', markersize=12, color='skyblue', linewidth=4)
    plt.plot( 'years', 'Wells_Fargo', data=df, marker='', color='red', linewidth=2)
    plt.plot( 'years', 'Bank_of_America', data=df, marker='', color='olive', linewidth=2, linestyle='dashed')
    plt.legend()
    plt.show()

    chase_assets.reverse()
    amer_assets.reverse()
    wells_assets.reverse()

    return chase_assets, amer_assets, wells_assets




# Problem 3
def prob3():
    """The Basketball Reference website at https://www.basketball-reference.com
    contains data on NBA athletes, including which player led different 
    categories for each season. For the past ten seasons, identify which player 
    had the most season points and find how many points they scored during that 
    season. Return a list of triples consisting of the season, the player, and 
    the points scored, ("season year", "player name", points scored).
    """
    
    #Create an iterable to loop through years
    years = [str(x) for x in range(2009,2019)]
    
    #Get the soup for each page and get the highest scorer from each page
    everyone = []
    for year in years:
        time.sleep(1)
        url = 'https://www.basketball-reference.com/leagues/NBA_' + year + '_leaders.html'
        soup = BeautifulSoup(requests.get(url).text, "html.parser")
        points = soup.find(string='Points').parent.parent.find(class_="who").next_sibling.next_sibling.string
        person = soup.find(string='Points').parent.parent.find(class_="who").a.string
        everyone.append((year, person, int(points)))
    
    return everyone



# Problem 4
def prob4(search_query):
    """Use Selenium to enter the given search query into the search bar of
    https://arxiv.org and press Enter. The resulting page has up to 25 links
    to the PDFs of technical papers that match the query. Gather these URLs,
    then continue to the next page (if there are more results) and continue
    gathering links until obtaining at most 100 URLs. Return the list of URLs.

    Returns:
        (list): Up to 100 URLs that lead directly to PDFs on arXiv.
    """
    #open chrome
    browser = webdriver.Chrome()
    
    #Try makes sure that no matter what, the driver will close at the end.
    try:
        #Make a search
        browser.get('https://arxiv.org')
        time.sleep(1)
        search_bar = browser.find_element_by_css_selector('#search-arxiv > div > input.keyword-field')
        search_bar.send_keys(search_query)  
        
        #Click Search
        browser.find_element_by_css_selector('#search-arxiv > div > input.btn-search-arxiv').click()
        time.sleep(2)
        
        #Search first page of results
        pages = [BeautifulSoup(browser.page_source, 'html.parser')]
        
        #Scrape next two search pages as well.
        #If there aren't any next pages, it's OK to stop; hence the try and except loop
        try:
            for _ in range(0,2):
                browser.find_element_by_css_selector('body > content > section > div > div > content > nav:nth-child(3) > a.pagination-next').click()    
                time.sleep(2)   
                pages.append(BeautifulSoup(browser.page_source, 'html.parser'))                 
        except:
            pass
    finally:
        browser.quit()
    
    all_titles = []
    #Now we parse out the soup!
    for page in pages:
        titles = page.find_all(class_='list-title level-left')
        all_titles += titles
    
    #Get the actual text
    words = []
    for title in all_titles:
        words.append(title.a['href'].replace('/abs/','/pdf/') + '.pdf')
        
    return words


# Problem 5
def prob5():
    """For each of the (at least) 600 problems in the archive at
    https://projecteuler.net/archives, record the problem ID and the number of
    people who have solved it. Return a list of IDs, sorted from largest to
    smallest by the number of people who have solved them. That is, the first
    entry in the list should be the ID of the most solved problem, and the
    last entry in the list should be the ID of the least solved problem.

    Returns:
        (list): problem IDs (as strings), from most solved to least solved.
    """
    #Creat iterable that can iterate through webpages
    page_num = [x for x in range(1,14)]
    
    #Get all the web pages in one go
    pages = []
    for num in page_num: 
        page = BeautifulSoup(requests.get('https://projecteuler.net/archives;page=' + str(num)).text, 'html.parser')
        pages.append(page)
        time.sleep(1)

    
    #Create a regular expression that matches numbers
    number = re.compile('^[0-9]+$')
    
    #Loop through Each page
    all_ids = []
    all_num_compl = []
    for page in pages:
        
        #Find the table in the page
        table = page.find(class_='grid')
        
        #Find the ids of the challenges
        ids = table.find_all(class_='id_column', string=number)
        
        #Find the number completed for each challenge
        num_compl = table.find_all(style='text-align:center;', string=number)
        
        #Get everything in a friendly format
        for id_ in ids:
            all_ids.append(int(id_.string))
        for num in num_compl:
            all_num_compl.append(int(num.string))
        
        #For safety make sure that corresponding lists are the same length
        assert len(ids) == len(num_compl)
    
    #complie the two into one list
    combined = list(zip(all_num_compl, all_ids))
    
    #Sort sorts first element first, and we want it descending
    combined.sort(reverse=True)
    
    print(combined)
    
    #return just the ids
    out = list(list(zip(*combined))[1])
    oout = []
    for o in out:
        oout.append(str(o))
    return oout
    
