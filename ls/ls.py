# -*- coding: utf-8 -*-
"""
Created on Tue Oct 23 19:39:21 2018

@author: tophe
"""

# lstsq_eigs.py
"""Volume 1: Least Squares and Computing Eigenvalues.
<Name>
<Class>
<Date>
"""

import numpy as np
from cmath import sqrt
from scipy import linalg as la
from matplotlib import pyplot as plt

#This is where I saved things in my computer: feel free to change this to 
#Suit your grading needs
filepath = 'C:/Users/tophe/OneDrive/Documents/labs_all/ls/'

# Problem 1
def least_squares(A, b):
    """Calculate the least squares solutions to Ax = b by using the QR
    decomposition.

    Parameters:
        A ((m,n) ndarray): A matrix of rank n <= m.
        b ((m, ) ndarray): A vector of length m.

    Returns:
        x ((n, ) ndarray): The solution to the normal equations.
    """
    Q, R = np.linalg.qr(A)
    right = (np.matrix.transpose(Q)).dot(b)
    
    xhat = la.solve_triangular(R, right)
    
    return xhat
    
least_squares(np.array([[1,2],[-1,3],[2,0]]), np.array([-5,-4,-2]).T) 

# Problem 2
def line_fit():
    """Find the least squares line that relates the year to the housing price
    index for the data in housing.npy. Plot both the data points and the least
    squares line.
    """
    year, index = np.load(filepath + "housing.npy").T

    #write your code here

    one = np.ones_like(year)
    
    X = np.vstack((one,year))

    intercept,slope = least_squares(X.T, index)
    
    print(slope, intercept)

    #Cool!

    plt.plot(year, index, 'k*')
    plt.plot(year, year*slope + intercept, '-')
    plt.show()



# Problem 3
def polynomial_fit():
    """Find the least squares polynomials of degree 3, 6, 9, and 12 that relate
    the year to the housing price index for the data in housing.npy. Plot both
    the data points and the least squares polynomials in individual subplots.
    """
    year, index = np.load(filepath + "housing.npy").T
    #find the domain here
    
    for i,n in enumerate([3, 6, 9, 12]):
        # Use least squares to compute the coefficients of the polynomial.
        coeffs = la.lstsq(np.vander(year,n),index)[0]
        domain = np.linspace(0,16)
    
        # Plot the polynomial and the data points in an individual subplot.
        plt.subplot(2,2,i+1)
        plt.plot(year, index, 'k*')
        plt.plot(domain, np.polyval(coeffs, domain), '-')
        plt.title(r"$n = {}$".format(n))
        # plt.axis([x.min(),x.max(),y.min(),y.max()])

    plt.suptitle("Solution to Problem 3")
    plt.show()    
    
    #Now I'm going to comare this to what I get with np.polyfit
    plt.clf()
    for i,n in enumerate([3, 6, 9, 12]):
        # Use least squares to compute the coefficients of the polynomial.
        coeffs = np.polyfit(year, index, n-1)
        domain = np.linspace(0,16)
    
        # Plot the polynomial and the data points in an individual subplot.
        plt.subplot(2,2,i+1)
        plt.plot(year, index, 'k*')
        plt.plot(domain, np.polyval(coeffs, domain), '-')
        plt.title(r"$n = {}$".format(n))
        # plt.axis([x.min(),x.max(),y.min(),y.max()])

    plt.suptitle("Comparison on Problem 3 with polyfit")
    plt.show()    
    
    #So that looks exactly the same!
    

#Problem 4 and plot_ellipse are optional
def plot_ellipse(a, b, c, d, e):
    """Plot an ellipse of the form ax^2 + bx + cxy + dy + ey^2 = 1."""
    theta = np.linspace(0, 2*np.pi, 200)
    cos_t, sin_t = np.cos(theta), np.sin(theta)
    A = a*(cos_t**2) + c*cos_t*sin_t + e*(sin_t**2)
    B = b*cos_t + d*sin_t
    r = (-B + np.sqrt(B**2 + 4*A)) / (2*A)

    plt.plot(r*cos_t, r*sin_t)
    plt.gca().set_aspect("equal", "datalim")

# Problem 4
def ellipse_fit():
    """Calculate the parameters for the ellipse that best fits the data in
    ellipse.npy. Plot the original data points and the ellipse together, using
    plot_ellipse() to plot the ellipse.
    """
    
    #Challenge accepted!

    x,y = np.load(filepath + "ellipse.npy").T
    
    xsq = x * x
    ysq = y * y
    xy = x * y
    
    A = np.vstack((xsq, x, xy, y, ysq))
    b = np.ones_like(x)
    
    
    a,b,c,d,e = least_squares(A.T,b)
    
    plot_ellipse(a,b,c,d,e)
    
    plt.plot(x, y, 'k*')
    
    
    
    

