# solutions.py
"""Volume 2: One-Dimensional Optimization.
Christopher Cook
Math 406R
11/16/2018
"""
import math
from scipy import optimize as opt
import numpy as np
import matplotlib.pyplot as plt

# Problem 1
def golden_section(f, a, b, tol=1e-5, maxiter=15):
    """Use the golden section search to minimize the unimodal function f.

    Parameters:
        f (function): A unimodal, scalar-valued function on [a,b].
        a (float): Left bound of the domain.
        b (float): Right bound of the domain.
        tol (float): The stopping tolerance.
        maxiter (int): The maximum number of iterations to compute.

    Returns:
        (float): The approximate minimizer of f.
        (bool): Whether or not the algorithm converged.
        (int): The number of iterations computed.
    """
    
    fi = (1.0 + 5.0**(0.5))/2.0
    x0 = (a + b)/2.0
    converged = False
    
    for i in range(1, maxiter + 1):
        c = (b-a)/fi
        asq = b - c
        bsq = a + c
        if f(asq) <= f(bsq):
            b = bsq
        else:
            a = asq
        x1 = (a+b)/2.0
        if abs(x0 - x1) < tol:
            converged=True
            break
        x0 = x1
        
    return x1, converged, i
    
"""
test function f(x) = e^x - 4x
"""
def ef(x):
    return (math.exp(x) - 4*x)

"""
test code
"""
'''
solution, unimprtant, filler = golden_section(ef, 0,3)
print(golden_section(ef, 0,3))
opt.golden(ef, brack=(0,3), tol=0.001)
'''

"""
Graph
"""
'''
eff = np.vectorize(ef)
domain = np.arange(0,3, 0.002)
y = eff(domain)
plt.plot(domain, y)
plt.plot(solution, ef(solution), 'rp', markersize=14)
plt.show()
'''

# Problem 2
def newton1d(df, d2f, x0, tol=1e-5, maxiter=15):
    """Use Newton's method to minimize a function f:R->R.

    Parameters:
        df (function): The first derivative of f.
        d2f (function): The second derivative of f.
        x0 (float): An initial guess for the minimizer of f.
        tol (float): The stopping tolerance.
        maxiter (int): The maximum number of iterations to compute.

    Returns:
        (float): The approximate minimizer of f.
        (bool): Whether or not the algorithm converged.
        (int): The number of iterations computed.
    """
    for itera in range(0,maxiter):
        x1 = x0 - df(x0)/d2f(x0)
        if abs(x1 - x0) < tol:
            return x1, True, itera + 1
        x0 = x1
    return x1, False, maxiter
'''
df = lambda x : 2*x + 5*np.cos(5*x)
d2f = lambda x : 2 - 25*np.sin(5*x)

print(newton1d(df, d2f, 0, maxiter=500))
opt.newton(df, x0=0, fprime=d2f, tol=1e-10, maxiter=500)
'''

# Problem 3
def secant1d(df, x0, x1, tol=1e-5, maxiter=15):
    """Use the secant method to minimize a function f:R->R.

    Parameters:
        df (function): The first derivative of f.
        x0 (float): An initial guess for the minimizer of f.
        x1 (float): Another guess for the minimizer of f.
        tol (float): The stopping tolerance.
        maxiter (int): The maximum number of iterations to compute.

    Returns:
        (float): The approximate minimizer of f.
        (bool): Whether or not the algorithm converged.
        (int): The number of iterations computed.
    """
    for i in range(0,maxiter):
        twoderiv = (df(x1) - df(x0)) / (x1 - x0)
        x2 = x1 - df(x1)/twoderiv
        if abs(x1 - x2) < tol:
            return x2, True, i + 1
        x0 = x1
        x1 = x2
    return x2, False, maxiter

'''
df = lambda x: 2*x + np.cos(x) + 10*np.cos(10*x)
print(secant1d(df, 0, -1, maxiter=500))
opt.newton(df, x0=0, tol=1e-10, maxiter=500)
'''

# Problem 4
def backtracking(f, Df, x, p, alpha=1, rho=.9, c=1e-4):
    """Implement the backtracking line search to find a step size that
    satisfies the Armijo condition.

    Parameters:
        f (function): A function f:R^n->R.
        Df (function): The first derivative (gradient) of f.
        x (float): The current approximation to the minimizer.
        p (float): The current search direction.
        alpha (float): A large initial step length.
        rho (float): Parameter in (0, 1).
        c (float): Parameter in (0, 1).

    Returns:
        alpha (float): Optimal step size.
    """
    dfp = Df(x).T.dot(p)
    print(dfp)
    fx = f(x)
    print(fx)
    
    while(np.all(f(x + alpha * p) > (fx + c * alpha * dfp))):
        alpha = alpha * rho
    return alpha
'''
from scipy.optimize import linesearch
from autograd import numpy as anp
from autograd import grad
# Get a step size for f(x,y,z) = x^2 + y^2 + z^2.
f = lambda x: x[0]**2 + x[1]**2 + x[2]**2
x = anp.array([150., .03, 40.]) # Current minimizer guesss.
p = anp.array([-.5, -100., -4.5]) # Current search direction.
phi = lambda alpha: f(x + alpha*p) # Define phi(alpha).
dphi = grad(phi)
alpha, _ = linesearch.scalar_search_armijo(phi, phi(0.), dphi(0.))
print(alpha)

print(backtracking(f, Df,x, p))

f = lambda x: x[0]**2 + x[1]**2 + x[2]**2
Df = lambda x: np.array([2*x[0], 2*x[1], 2*x[2]])
'''