# regular_expressions.py
"""Volume 3: Regular Expressions.
Christopher Cook
MATH 406 R
21/9/2018
"""

import re

# Problem 1
def prob1():
    """Compile and return a regular expression pattern object with the
    pattern string "python".

    Returns:
        (_sre.SRE_Pattern): a compiled regular expression pattern object.
    """
    return re.compile('python')
        
# Problem 2
def prob2():
    """Compile and return a regular expression pattern object that matches
    the string "^{@}(?)[%]{.}(*)[_]{&}$".

    Returns:
        (_sre.SRE_Pattern): a compiled regular expression pattern object.
    """
    return re.compile(r'\^\{@\}\(\?\)\[%\]\{\.\}\(\*\)\[_\]\{&\}\$')

# Problem 3
def prob3():
    """Compile and return a regular expression pattern object that matches
    the following strings (and no other strings).

        Book store          Mattress store          Grocery store
        Book supplier       Mattress supplier       Grocery supplier

    Returns:
        (_sre.SRE_Pattern): a compiled regular expression pattern object.
    """
    return re.compile('^((Book)|(Matress)|(Grocery)) ((store)|(supplier))$')



# Problem 4
def prob4():
    """Compile and return a regular expression pattern object that matches
    any valid Python identifier.

    Returns:
        (_sre.SRE_Pattern): a compiled regular expression pattern object.
        $
    """
    return re.compile(r"^[a-zA-Z_](\w)*( )*(=( )*(((\d*\.(\d)*)|('[^']*'))|([a-zA-Z_](\w)*)( )*))?$")




# Problem 5
def prob5(code):
    """Use regular expressions to place colons in the appropriate spots of the
    input string, representing Python code. You may assume that every possible
    colon is missing in the input string.

    Parameters:
    code (str): a string of Python code without any colons.

    Returns:
    (str): code, but with the colons inserted in the right places.
    """
    
    pattern1 = "(if|elif|for|while|with|def|class)( (([a-zA-Z_](\w)*)|(\d+(\.(\d)+)?)|('[^']*'))( *)(==|>=|<=|<|>)( *)(([a-zA-Z_](\w)*)|(\d+(\.(\d)+)?)|('[^']*')))$"
    pattern2 = "(else|try|finally)$"
    pattern3 = "(except)( (([a-zA-Z_](\w)*)|(\d+(\.(\d)+)?)|('[^']*'))( *)(==|>=|<=|<|>)( *)(([a-zA-Z_](\w)*)|(\d+(\.(\d)+)?)|('[^']*')))?$"
    repl = r"\1\2:"
    repl2 = r"\1:"
    
    block1 = re.compile(pattern1, re.MULTILINE)
    block2 = re.compile(pattern2, re.MULTILINE)
    block3 = re.compile(pattern3, re.MULTILINE)
    
    temp1 =  block1.sub(repl,code)
    temp2 =  block2.sub(repl2,temp1)
    return block3.sub(repl,temp2)


# Problem 6
def prob6(filname="fake_contacts.txt"):
    """Use regular expressions to parse the data in the given file and format
    it uniformly, writing birthdays as mm/dd/yyyy and phone numbers as
    (xxx)xxx-xxxx. Construct a dictionary where the key is the name of an
    individual and the value is another dictionary containing their
    information. Each of these inner dictionaries should have the keys
    "birthday", "email", and "phone". In the case of missing data, map the key
    to None.

    Returns:
        (dict): a dictionary mapping names to a dictionary of personal info.
    """

    raise NotImplementedError("Problem 6 Incomplete")

