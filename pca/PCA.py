import numpy as np
from collections import Counter
from math import log
import string
from scipy import sparse
from scipy.sparse import linalg as sparla
from matplotlib import pyplot as plt
from scipy import linalg as la
import sklearn.datasets as datasets

def PCA_Code():
    # load iris data
    iris = datasets.load_iris()
    
    X = iris['data']
        
    #your code goes here
    X = X - X.mean(axis=0)
    
    
    U,S,V = la.svd(X, full_matrices = False)
    Yhat = U[:,0:2].dot(np.diag(S[0:2]))
    
    
    # plot results
    setosa = iris.target==0
    versicolor = iris.target==1
    virginica = iris.target==2
    p1, p2 = Yhat[:,0], Yhat[:,1]
    plt.scatter(p1[setosa],p2[setosa], marker='.', color='blue', label='Setosa')
    plt.scatter(p1[versicolor],p2[versicolor], marker='.', color='red', label='Versicolor')
    plt.scatter(p1[virginica],p2[virginica], marker='.', color='green', label='Virginica')
    plt.legend(loc=2)
    plt.ylim([-4,5])
    plt.xlim([-4,4])
    plt.xlabel("First Principal Component")
    plt.ylabel("Second Principal Component")
    plt.show()

if __name__ == "__main__":
    PCA_Code()
