# solutions.py
"""Volume 2: Newton and Quasi-Newton Methods.
Christopher Cook
406R
12/1/2018
"""
from scipy import linalg as la
from scipy import optimize as opt
import pandas as pd
import numpy as np
from matplotlib import pyplot as plt
import time
import pickle
import math
#%matplotlib inline


# Problem 1
def newton(Df, D2f, x0, tol=1e-5, maxiter=20):
    """Use Newton's method to minimize a function f:R^n -> R.

    Parameters:
        Df (function): The first derivative of f. Accepts and returns a NumPy
            array of shape (n,).
        D2f (function): The second dirivative (Hessian) of f. Accepts a NumPy
            array of shape (n,) and returns a NumPy array of shape (n,n).
        x0 ((n,) ndarray): The initial guess.
        tol (float): The stopping tolerance.
        maxiter (int): The maximum number of iterations to compute.

    Returns:
        ((n,) ndarray): The approximate optimizer of f.
        (bool): Whether or not the algorithm converged.
        (int): The number of iterations computed.
    """
    for i in range(0, maxiter):
        Dfx = Df(x0)
        D2fx = D2f(x0)
        if la.norm(Dfx, np.inf) < tol:
            return x0, True, i + 1
        
        x0 = x0 - la.solve(D2fx, Dfx) 
    return x0, False, maxiter

#print(newton(opt.rosen_der, opt.rosen_hess, np.array([2,2])))


# Problem 2
def bfgs(Df, x0, tol=1e-5, maxiter=80):
    """Use BFGS to minimize a function f:R^n -> R.

    Parameters:
        Df (function): The first derivative of f. Accepts and returns a NumPy
            array of shape (n,).
        x0 ((n,) ndarray): The initial guess.
        tol (float): The stopping tolerance.
        maxiter (int): The maximum number of iterations to compute.

    Returns:
        ((n,) ndarray): The approximate optimizer of f.
        (bool): Whether or not the algorithm converged.
        (int): The number of iterations computed.
    """
    A = np.identity(len(x0))
    Dfx = Df(x0).T
    for i in range(0, maxiter):

        #End if Df is small enough
        if la.norm(Dfx, np.inf) < tol:
            return x0, True, i + 1
        
        #update guess for x
        x1 = x0 - A.dot(Dfx)
        Dfx1 = Df(x1).T
        
        #compute Inermediaries
        s = x1 - x0
        #print(s)
        y = Dfx1 - Dfx
        #print(y)
        insy = s.dot(y)
        #print('insy',insy)
        if insy == 0.0:
            #Try a slightly different starting point
            return x0, False, i+1
            
        # Update A    
        #print('a', A)
        A = A + ((insy + y.T.dot(A).dot(y)) * np.outer(s,s)) / (insy*insy) - ((A.dot(np.outer(y,s)) + np.outer(s,y).dot(A)) / insy)
        #A = A + np.outer(s,s) / insy - A.dot(np.outer(y,y)).dot(A) / ((y.dot(A)).dot(y))
        #print('A', A)
        
        #Update Df and X0
        Dfx = Dfx1
        x0 = x1
    return x0, False, maxiter

#print(bfgs(opt.rosen_der, np.array([1.9,2]), maxiter=100))


# Problem 3
def prob3(N=100):
    """Compare newton(), bfgs(), and scipy.optimize.fmin_bfgs() by repeating
    the following N times.
        1. Sample a random initial guess x0 from the 2-D uniform distribution
            over [-3,3]x[-3,3].
        2. Time (separately) newton(), bfgs(), and scipy.optimize.bfgs_fmin()
            for minimizing the Rosenbrock function with an initial guess of x0.
        3. Record the number of iterations from each method.
    Plot the computation times versus the number of iterations with a log-log
    scale, using different colors for each method.
    """
    Df = opt.rosen_der
    D2f = opt.rosen_hess
    
    Bfgs = []
    tBfgs = []
    Newton = []
    tNewton = []
    NpNewton = []
    tNpNewton = []
    
    for i in range(1,N+1):
        x0 = np.random.uniform(low=-3.0, high=3.0, size=(2,))
        t0 = time.time()
        Bfgs.append(bfgs(Df, x0, tol=1e-5, maxiter=200)[2])
        t1 = time.time()
        tBfgs.append(t1-t0)
        t0 = time.time()
        Newton.append(newton(Df, D2f, x0, tol=1e-5, maxiter=200)[2])
        t1 = time.time()
        tNewton.append(t1-t0)
        t0 = time.time()
        NpNewton.append(len(opt.fmin_bfgs(opt.rosen,[0,0],fprime=Df,retall=True, disp=False)[1]))
        t1 = time.time()
        tNpNewton.append(t1-t0)

    plt.scatter(tBfgs, Bfgs, label='BFGS', alpha=0.3)
    plt.scatter(tNewton, Newton, label='Newton', alpha=0.3)
    plt.scatter(tNpNewton, NpNewton, label = 'np.optimize.fmin_bfgs', alpha=0.3)
    plt.xscale('log')
    plt.yscale('log')
    plt.legend(loc='bottom left')
    #res = pd.DataFrame({'BFGS': Bfgs, 'Newton': Newton, 'np.optimize.fmin_bfgs': NpNewton,'BFGS time': tBfgs, 'Newton time': tNewton, 'np.optimize.fmin_bfgs time': tNpNewton})
    #res.plot(kind='scatter', x='BFGS time', y='BFGS')
    
    #return Bfgs, Newton, NpNewton

#prob3()


# Problem 4
def gauss_newton(J, r, x0, tol=1e-5, maxiter=10):
    """Solve a nonlinear least squares problem with the Gauss-Newton method.

    Parameters:
        J (function): Jacobian of the residual function. Accepts a NumPy array
            of shape (n,) and returns a NumPy array of shape (m,n).
        r (function): Residual vector function. Accepts a NumPy array of shape
            (n,) and returns an array of shape (m,).
        x0 ((n,) ndarray): The initial guess.
        tol (float): The stopping tolerance.
        maxiter (int): The maximum number of iterations to compute.

    Returns:
        ((n,) ndarray): The approximate optimizer of f.
        (bool): Whether or not the algorithm converged.
        (int): The number of iterations computed.
    """
    for i in range(0, maxiter):
        jx0 = J(x0)
        x1 = x0 - la.inv(jx0.T.dot(jx0)).dot(jx0.T.dot(r(x0)))
        if abs(la.norm(x1 - x0, np.inf)) < tol:
            return x1, True, i + 1
        x0 = x1
    return x0, False, maxiter

'''
import numpy as np
from matplotlib import pyplot as plt
# Generate random data for t = 0, 1, ..., 10.
T = np.arange(10)
y = 3*np.sin(0.5*T)+ 0.5*np.random.randn(10) # Perturbed data.
# Define the model function and the residual (based on the data).
model = lambda x, t: x[0]*np.sin(x[1]*t) # phi(x,t)
residual = lambda x: model(x, T) - y # r(x) = phi(x,t) - y
# Define the Jacobian of the residual function, computed by hand.
def jac(x, T=np.arange(10)):
    out = []
    for t in T: 
        out.append(np.column_stack((np.sin(x[1]*t), x[0]*t*np.cos(x[1]*t))))
    #print(tuple(out))
    return np.vstack(tuple(out))
x0 = np.array([2.5,.6])
x, conv, niters = gauss_newton(jac, residual, x0, tol=1e-3, maxiter=100)

minx = opt.leastsq(func=residual, x0=np.array([2.5,.6]), Dfun=jac)

'''

# Problem 5
def prob5(filename="population.npy"):
    """Load the data from the given file. Fit the data to an exponential model

        phi(x1, x2, x3, t) = x1 exp(x2(t + x3))

    and to a logistic model

        phi(x1, x2, x3, t) = x1 / (1 + exp(-x2(t + x3))).

    Plot the resulting curves along with the data points.
    """
    population = np.load(filename)
    
    exp_mod = lambda x, t: x[0] * np.exp(x[1] * (t + x[2]))
    exp_res = lambda x: exp_mod(x, population[:,0]) - population[:,1]
    exp_jac = lambda x: np.column_stack([np.exp(x[1] * (population[:,0] + x[2])), 
                                        x[0] * (population[:,0] + x[2]) * np.exp(x[1] * (population[:,0] + x[2])),
                                         x[0] * x[1] * np.exp(x[1] * (population[:,0] + x[2]))])
    
    x0 = (150, .4, 15)
    
    minx = opt.leastsq(func=exp_res, x0=x0,Dfun=exp_jac)
    plt.figure(0)
    plt.scatter(x=population[:,0], y=population[:,1])
    dom = np.linspace(0,15)
    rng = exp_mod(minx[0], dom)
    plt.scatter(x=dom, y=rng)
    
    log_mod = lambda x, t: x[0] / (1 + np.exp(-x[1] * (t + x[2])))
    log_res = lambda x: log_mod(x, population[:,0]) - population[:,1]
    log_jac = lambda x: np.column_stack([1/(1 + np.exp(-x[1] * (population[:,0] + x[2]))),
                                         x[0] * (population[:,0] + x[2]) * np.exp(-x[1] * (population[:,0] + x[2])) /((1 + np.exp(-x[1] * (population[:,0] + x[2])))**2),
                                        x[0] * (x[1]) * np.exp(-x[1] * (population[:,0] + x[2]))/((1 + np.exp(-x[1] * (population[:,0] + x[2])))**2)  ])
    x0 = (150, .4, -15)
    minx = opt.leastsq(func=log_res, x0=x0,Dfun=log_jac)
    
    plt.figure(1)
    plt.scatter(x=population[:,0], y=population[:,1])
    dom = np.linspace(0,15)
    rng = log_mod(minx[0], dom)
    plt.scatter(x=dom, y=rng)
    return minx
'''  
print(prob5())
'''